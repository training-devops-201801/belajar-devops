# Aplikasi Contoh untuk Deployment #

## Beberapa Perintah Docker ##

* Melihat image yang ada di cache local

    ```
    docker image ls
    ```

* Membuat private network antar container

    ```
    docker network create belajarnet
    ```

* Melihat seluruh container

    ```
    docker ps -a
    ```

* Melihat container yang tidak berjalan

    ```
    docker ps -a -f status=exited
    ```

* Menghapus container yang tidak berjalan

    ```
    docker ps -aq --no-trunc -f status=exited | xargs docker rm
    ```

## Menjalankan MySQL Server di Docker ##

Menjalankan MySQL container menggunakan image `mysql` dengan versi `5`. Container ini akan terhubung ke jaringan `belajarnet`.

```
docker run --detach --name mysql-belajar \
           --env-file env-docker.txt \
           --network belajarnet \
           --publish 3306:3306 \
           --volume /root/db-belajar:/var/lib/mysql \
           mysql:5
```

Mematikan MySQL container

```
docker stop mysql-belajar
```

Menghapus MySQL container

```
docker rm mysql-belajar
```

## Membuat Docker Image ##

```
docker build -t belajar-devops .
```

## Menjalankan Aplikasi yang sudah di-Dockerize ##

```
docker run --name belajar-devops \
           --env SPRING_PROFILES_ACTIVE=docker \
           --network belajarnet \
           --publish 8080:8080 \
           belajar-devops:latest
```

## Menjalankan Rangkaian Container dengan Docker Compose ##

```
docker-compose up
```
