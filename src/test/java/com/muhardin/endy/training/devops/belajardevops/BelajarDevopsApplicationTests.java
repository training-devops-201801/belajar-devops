package com.muhardin.endy.training.devops.belajardevops;

import com.muhardin.endy.training.devops.belajardevops.dao.ProdukDao;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BelajarDevopsApplicationTests {
    @Autowired private ProdukDao produkDao;
    
    @Test
    public void testJumlahProduk(){
        Assert.assertEquals(5, produkDao.count());
    }
}
