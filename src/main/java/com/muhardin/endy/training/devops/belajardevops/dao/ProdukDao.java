package com.muhardin.endy.training.devops.belajardevops.dao;

import com.muhardin.endy.training.devops.belajardevops.entity.Produk;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProdukDao extends PagingAndSortingRepository<Produk, String>{
    
}
