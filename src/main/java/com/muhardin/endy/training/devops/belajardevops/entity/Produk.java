package com.muhardin.endy.training.devops.belajardevops.entity;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Entity @Data
public class Produk {
    
    @Id
    private String id;
    
    @NotNull @NotEmpty
    private String kode;
    
    @NotNull @NotEmpty
    private String nama;
    
    @NotNull @Min(1)
    private BigDecimal harga;
}
