package com.muhardin.endy.training.devops.belajardevops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelajarDevopsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelajarDevopsApplication.class, args);
	}
}
