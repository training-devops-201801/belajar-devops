package com.muhardin.endy.training.devops.belajardevops.controller;

import com.muhardin.endy.training.devops.belajardevops.dao.ProdukDao;
import com.muhardin.endy.training.devops.belajardevops.entity.Produk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/produk")
public class ProdukController {
    
    @Autowired private ProdukDao produkDao;
    
    @GetMapping("/")
    public Page<Produk> dataProduk(Pageable page){
        return produkDao.findAll(page);
    }
}
