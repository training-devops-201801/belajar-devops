create table produk (
  id varchar(36),
  kode varchar(100) not null, 
  nama varchar(255) not null,
  harga numeric(19,2) not null,
  primary key (id), 
  unique (kode)
) ;