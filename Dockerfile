FROM openjdk:8-alpine
ADD target/belajar-devops-0.0.1-SNAPSHOT.jar /opt/app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/opt/app.jar"]
