# Perintah Kubernetes Sehari-hari #

* Menjalankan file konfigurasi

    ```
    kubectl create -f namafile.yml
    ```

* Melihat daftar persistent volume

    ```
    kubectl get pv
    ```

* Melihat semua object aplikasi `belajar-devops`

    ```
    kubectl get pod,svc,deployments,endpoints -l aplikasi=belajar-devops
    ```

* Melihat semua object `frontend` aplikasi `belajar-devops`

    ```
    kubectl get pod,svc,deployments,endpoints -l aplikasi=belajar-devops,tier=frontend
    ```

* Melihat log stream pod (untuk troubleshoot kalau ada error)

    ```
    kubectl logs -f <nama pod>
    ```

* Menghapus satu pod/service/deployment

    ```
    kubectl delete <nama pod/service/deployment>
    ```

* Menghapus seluruh object `frontend`

    ```
    kubectl delete pods,services -l aplikasi=belajar-devops,tier=frontend
    ```

* Menambah replica deployment `belajar-devops-app`

    ```
    kubectl scale --replicas=10 belajar-devops-app
    ```

## Referensi ##

* https://kubernetes.io/docs/reference/kubectl/cheatsheet/